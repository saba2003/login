package com.example.emailpassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class SignUpActivity : AppCompatActivity() {

    private lateinit var editTextEmail:TextInputEditText
    private lateinit var editTextPassword:TextInputEditText
    private lateinit var editTextPassword2: TextInputEditText
    private lateinit var buttonSignIn:Button
    private lateinit var buttonBack:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        init()

        signupListeners()

    }

    private fun init() {

        editTextEmail= findViewById(R.id.editTextEmail)
        editTextPassword= findViewById(R.id.editTextPassword)
        editTextPassword2= findViewById(R.id.editTextPassword2)
        buttonSignIn= findViewById(R.id.buttonSignIn)
        buttonBack= findViewById(R.id.buttonBack)


    }

    private fun signupListeners() {

        buttonBack.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        buttonSignIn.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "min 1 upper-case symbol", Toast.LENGTH_SHORT).show()
            }

            if (!password.matches(".*[a-z].*".toRegex())) {
                Toast.makeText(this, "min 1 lower-case symbol", Toast.LENGTH_SHORT).show()
            }

            if (!password.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "min 1 number", Toast.LENGTH_SHORT).show()
            }

            if (!email.matches(".*[@].*".toRegex())) {
                Toast.makeText(this, "incorrect email!", Toast.LENGTH_SHORT).show()
            }

            if (password != editTextPassword2.text.toString()){
                Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(password.isEmpty() || password.length < 9) {
                Toast.makeText(this, "Incorrect Password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }


        }
    }

}