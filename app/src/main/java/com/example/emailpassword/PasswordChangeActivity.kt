package com.example.emailpassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var editTextPassword:EditText
    private lateinit var editTextPassword2:EditText
    private lateinit var buttonPasswordChange:Button
    private lateinit var buttonBack:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)

        init()

        signupListeners()

    }

    private fun init() {

        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        buttonPasswordChange = findViewById(R.id.buttonPasswordChange)
        buttonBack = findViewById(R.id.buttonBack)


    }

    private fun signupListeners() {

        buttonBack.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
            finish()
        }


        buttonPasswordChange.setOnClickListener {

            val newPassword = editTextPassword.text.toString()
            val confirmPassword = editTextPassword2.text.toString()

            if(newPassword.isEmpty() || newPassword.length < 9) {
                Toast.makeText(this, "Incorrect Password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "min 1 upper-case symbol", Toast.LENGTH_SHORT).show()
            }

            if (!newPassword.matches(".*[a-z].*".toRegex())) {
                Toast.makeText(this, "min 1 lower-case symbol", Toast.LENGTH_SHORT).show()
            }

            if (!newPassword.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "min 1 number", Toast.LENGTH_SHORT).show()
            }

            if (newPassword != confirmPassword){
                Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, ProfileActivity::class.java))
                        finish()
                    }else {
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }

        }

    }
}